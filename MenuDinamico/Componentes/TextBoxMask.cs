﻿using LibraryEOM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MenuDinamico.Componentes
{
    public class TextBoxMask : TextBox
    {

        public Mascaras.Masks Mask { set; get; }

        public TextBoxMask()
        {
            this.TextChanged += new TextChangedEventHandler(MaskedTextBox_TextChanged);
        }

        void MaskedTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.CaretIndex = this.Text.Length;

            var tbEntry = sender as TextBoxMask;

            if (tbEntry != null && tbEntry.Text.Length > 0)
            {
                tbEntry.Text = Mascaras.FormatosNumeros(tbEntry.Text, tbEntry.Mask);

            }
        }
    }

       
}
