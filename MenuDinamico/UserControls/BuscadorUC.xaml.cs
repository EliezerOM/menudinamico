﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MenuDinamico.UserControls
{
    /// <summary>
    /// Lógica de interacción para BuscadorUC.xaml
    /// </summary>
    public partial class BuscadorUC : UserControl
    {

        #region Propiedades
        [System.ComponentModel.DefaultValue(null)]
        public object MenuItems
        {
            get { return (object)this.GetValue(MenuItemsProperty); }
            set { this.SetValue(MenuItemsProperty, value); }
        }

        [System.ComponentModel.DefaultValue(350)]
        public int MaxPresentationFilter
        {
            get { return (int)this.GetValue(MaxPresentationFilterProperty); }
            set { this.SetValue(MaxPresentationFilterProperty, value); }
        }



        #endregion


        #region DependencyProperties
        public static readonly DependencyProperty MaxPresentationFilterProperty = DependencyProperty.Register(
            nameof(MaxPresentationFilter),
            typeof(int),
            typeof(BuscadorUC),
            new FrameworkPropertyMetadata(default(int), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, MaxPresentationFilterChangedCallback)
            );



        public static readonly DependencyProperty MenuItemsProperty =
   DependencyProperty.Register(
       nameof(MenuItems),
       typeof(object),
       typeof(BuscadorUC),
       new FrameworkPropertyMetadata(default(object), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, MenuItemsChangedCallback));
        #endregion
        public BuscadorUC()
        {
            InitializeComponent();
      
        }

        private void TxtBuscar_MouseEnter(object sender, MouseEventArgs e)
        {
            focustxtBuscador();
        }

        private void TxtBuscar_GotFocus(object sender, RoutedEventArgs e)
        {
            focustxtBuscador();
        }

    

        private void focustxtBuscador()
        {

       
        }

        private void TxtBuscar_LostFocus(object sender, RoutedEventArgs e)
        {
            focustxtBuscador();
            //DataPresentacion.IsOpen = false;
        }

        #region Metodos
        public static void MenuItemsChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var BuscadorUC = (BuscadorUC)dependencyObject;
            
           
        }

        public static void MaxPresentationFilterChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var BuscadorUC = (BuscadorUC)dependencyObject;
            var s=(int)dependencyPropertyChangedEventArgs.NewValue;
           


        }
        #endregion


    }
}
