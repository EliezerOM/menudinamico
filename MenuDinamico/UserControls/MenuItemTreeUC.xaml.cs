﻿using MenuDinamico.ViewModels;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MenuDinamico.UserControls
{
    [ImplementPropertyChanged]
    /// <summary>
    /// Lógica de interacción para MenuItemTreeUC.xaml
    /// </summary>
    public partial class MenuItemTreeUC : UserControl
    {
  
        

        /// <summary>
        /// Itemsource del Menu Arbol
        /// </summary>
        public ObservableCollection<MenuP> MenuItemTree
        {
            get { return (ObservableCollection<MenuP>)this.GetValue(MenuItemTreeProperty); }
            set { this.SetValue(MenuItemTreeProperty, value); }
        }

        /// <summary>
        /// Activa el modo Acordion
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        public Boolean Accordion
        {
            get { return (Boolean)this.GetValue(AccordionProperty); }
            set { this.SetValue(AccordionProperty, value); }
        }

        [System.ComponentModel.DefaultValue(null)]
        public static object SelectItemMenu (DependencyObject element, object value)
        {
           
            return value;
        }

        private static MenuItemTreeUC menuItemTreeUC { get; set; } = null;

        #region DependencyProperty

        public static readonly DependencyProperty SelectItemMenuProperty = DependencyProperty.RegisterAttached
            (
            nameof(SelectItemMenu),
            typeof(object),
            typeof(MenuItemTreeUC),
            new FrameworkPropertyMetadata(default(object),FrameworkPropertyMetadataOptions.Inherits)
            );

        public static readonly DependencyProperty MenuItemTreeProperty =
          DependencyProperty.Register(
              nameof(MenuItemTree),
              typeof(ObservableCollection<MenuP>),
              typeof(MenuItemTreeUC),
              //new PropertyMetadata(null),
       new FrameworkPropertyMetadata(default(object), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, MenuItemTreePropertyChangedCallback));


      public static readonly DependencyProperty AccordionProperty =
         DependencyProperty.Register(
             nameof(Accordion),
             typeof(Boolean),
             typeof(MenuItemTreeUC),
      //new PropertyMetadata(null),
      new FrameworkPropertyMetadata(default(Boolean), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, AccordionChangedCallback));
        #endregion

        
        public MenuItemTreeUC()
        {
          
            InitializeComponent();
            
                        
        }

        
        public static void AccordionChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var MenuItemTreeUC = (MenuItemTreeUC)dependencyObject;

            ObservableAccordeon(MenuItemTreeUC, MenuItemTreeUC.Accordion);
        }

        public static void ObservableAccordeon(MenuItemTreeUC pMenu,bool pActiveAccordeon)
        {  //Solo un expander activo


            if (pActiveAccordeon)
            {
                int vCount = 0;
                if (pMenu.lolo.Children.Count > 0)
                {
                    foreach (var item in pMenu.lolo.Children)
                    {
                        if (item.GetType().Name.ToUpper() == "EXPANDER")
                        {
                            if (((Expander)item).Focus() && ((Expander)item).IsExpanded && vCount < 1)
                            {
                                ((Expander)item).IsExpanded = true;
                                vCount++;
                            }
                            else
                            {
                                ((Expander)item).IsExpanded = false;
                            }
                        }
                    }
                }
            }
        }

        public static void ObservableAccordeon(Expander pExpander, bool pActiveAccordeon)
        {  //Solo un expander activo

            if (pActiveAccordeon)
            {
           
                if (pExpander!=null)
                {
                    if (menuItemTreeUC !=null){
                        foreach (var item in menuItemTreeUC.lolo.Children)
                        {
                            if (item.GetType().Name.ToUpper() == "EXPANDER")
                            {
                                if (!((Expander)item).Equals(pExpander))
                                {
                                    ((Expander)item).IsExpanded = false;
                                }
                            }
                        } }
                }
               
            }
        }

        public static void MenuItemTreePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs) {
            var MenuItemTree = (MenuItemTreeUC)dependencyObject;
            menuItemTreeUC = MenuItemTree;
            foreach (var item in crearMenuArbol(MenuItemTree.MenuItemTree))
            {
                MenuItemTree.lolo.Children.Add(item);
            }

        }
        private static List<Expander> crearMenuArbol(ObservableCollection<MenuP> pMenu)
        {          
            StackPanel stackPanel;
            StackPanel stackPanelHeader;
            Expander expander;
           List<Expander> Expanders = new List<Expander>();


            //Montar Cabeceras
            foreach (var item in pMenu)
            {
                var ContentListView = FillListViewMenu(item);

                ScrollViewer scrollViewer = new ScrollViewer() 
                {
                    MaxHeight=400,MinWidth=200, 
                    VerticalScrollBarVisibility=ScrollBarVisibility.Auto,
                    HorizontalScrollBarVisibility=ScrollBarVisibility.Disabled,
                    IsEnabled=true
                };

                stackPanel = new StackPanel() { Margin=new Thickness(10)};
                stackPanelHeader = new StackPanel() { Orientation=Orientation.Horizontal} ;

                if (item.MenuItems.Count > 0)
                {
                    TextBox textBox = new TextBox()
                    {
                        Cursor = Cursors.IBeam,
                        Margin = new Thickness(0, 0, 0, 10),
                        MinWidth = 250

                    };
                    textBox.TextChanged += TextBox_TextChanged;
                    textBox.Tag = item.menuHeader;


                    Grid gridBuscador = new Grid();

                    gridBuscador.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                    gridBuscador.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Auto) });

                    MaterialDesignThemes.Wpf.PackIcon packIcon = new MaterialDesignThemes.Wpf.PackIcon()
                    {
                        Kind = MaterialDesignThemes.Wpf.PackIconKind.Search,
                        Margin = new Thickness(0, 20, 10, 0),
                        Foreground = Brushes.Red
                    };

                    Grid.SetColumn(packIcon, 0);

                    TextBlock textBlock = new TextBlock() { Text = "Buscar Aplicación" };
                    Grid.SetColumn(textBox, 1);

                    gridBuscador.Children.Add(packIcon);
                    gridBuscador.Children.Add(textBox);


                    MaterialDesignThemes.Wpf.HintAssist.SetIsFloating(textBox, true);
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(textBox, textBlock);

                    stackPanel.Children.Add(gridBuscador);
                }

                scrollViewer.Content = ContentListView;
                menuItemTreeUC.RegisterName(scrollViewer.Name = item.menuHeader + "Scroll", scrollViewer);

                stackPanel.Children.Add(scrollViewer);
                menuItemTreeUC.RegisterName(ContentListView.Name= item.menuHeader+"List", ContentListView);

                if (item.iconMenuVisible==Visibility.Visible) {

                    MaterialDesignThemes.Wpf.PackIcon icono = new MaterialDesignThemes.Wpf.PackIcon()
                    {
                        Margin = new Thickness(0, 0, 10, 0)
                    };

                    icono.Kind = (MaterialDesignThemes.Wpf.PackIconKind)Enum.Parse(typeof(MaterialDesignThemes.Wpf.PackIconKind), item.Icono);
                    stackPanelHeader.Children.Add(icono); 
                }
               
                    stackPanelHeader.Children.Add(new TextBlock { Text = item.menuHeader });

                expander = new Expander() { Header = stackPanelHeader, Content = stackPanel };
                expander.Expanded += Expander_Expanded;
                Expanders.Add(expander);



            }

            return Expanders;
        }

        private static  void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = (TextBox)sender;
        



        }
      

    private static void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            if (menuItemTreeUC !=null) 
            {
                ObservableAccordeon((Expander)sender, menuItemTreeUC.Accordion);
            }
        }

        
        private static ListView FillListViewMenu(MenuP MenuP)
        {
            ListView listView = new ListView() 
            {
                Margin = new Thickness(10, 0, 0, 0),
                Background=Brushes.Transparent,
                BorderThickness=new Thickness(0),
                Padding=new Thickness(0)
            };
            if (MenuP.MenuItems != null)
            {
                foreach (var item2 in MenuP.MenuItems)
                {
                    if (item2.MenuItems != null)
                    {
                        var vContent = FillListViewMenu(item2);

                        Expander expander = new Expander()
                        {
                            Header = new TextBlock()
                            {
                                Text = item2.menuHeader,
                                FontWeight = FontWeights.SemiBold
                            },
                            Content = vContent,
                            MinWidth = 150,
                            Padding = new Thickness(0),
                            BorderBrush = Brushes.DarkGray,
                            BorderThickness = new Thickness(0, 0, 0, .15),
                            Background = Brushes.Transparent,
                            HorizontalAlignment = HorizontalAlignment.Stretch,
                            Cursor = Cursors.Hand,
                            SnapsToDevicePixels = true,
                            Tag = item2.menuHeader
                        };
                        MaterialDesignThemes.Wpf.ExpanderAssist.SetDownHeaderPadding(expander, new Thickness(0));
                        MaterialDesignThemes.Wpf.ExpanderAssist.SetLeftHeaderPadding(expander, new Thickness(0));
                        MaterialDesignThemes.Wpf.ExpanderAssist.SetRightHeaderPadding(expander, new Thickness(0));
                        MaterialDesignThemes.Wpf.ExpanderAssist.SetUpHeaderPadding(expander, new Thickness(0));

                        listView.Items.Add(expander);
                        listView.ScrollIntoView(expander);

                    }
                    else 
                    {
                        ListViewItem listViewItem = new ListViewItem() { Content = item2.menuHeader, Cursor = Cursors.Hand, ToolTip = item2.menuHeader, DataContext = item2,Tag=item2.menuHeader };
                        listViewItem.Selected += ListViewItem_Selected;
                        listView.Items.Add(listViewItem);

                    }

                }
            }
            return listView;
        }

        private static void ListViewItem_Selected(object sender, RoutedEventArgs e)
        {
           
            SelectItemMenu(((ListViewItem)sender), ((ListViewItem)sender).DataContext);
        }
    }
}
