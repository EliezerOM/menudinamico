﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Windows.Controls;
using MenuDinamico.comandos;
using PropertyChanged;
using System.Windows.Media;
using System.Diagnostics;
using System.Windows.Markup;
using System.Threading;
using MaterialDesignThemes.Wpf;
using MenuDinamico.Models;
using System.Data;
using static MenuDinamico.ViewModels.MenuVM;
using System.Collections;
using System.Data.Linq.Mapping;

namespace MenuDinamico.ViewModels
{
    public static class cl {
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First());
        }

        public static bool ExistsBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First()).Any();
        }
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> original)
        {
            var s = original;
            return new ObservableCollection<T>(original);
        }

    

        public static ObservableCollection<object> ToObservableCollection(this IEnumerable original)
        {
            return new ObservableCollection<object>(original.Cast<object>());
        }

        public static DataTable ToDataTable(this DataRow pDataRow)
        {
            DataTable vTable = pDataRow.Table.Clone();
            DataRow vDataRow = vTable.NewRow();
            vDataRow.ItemArray = pDataRow.ItemArray;
            vTable.Rows.Add(vDataRow);
            return vTable;
           
        }
       
        public static IEnumerable<MenuP> ConvertToEnumarable(this DataTable pData,bool pHeader=false) 
        {
            ObservableCollection<MenuP> vMenu = new ObservableCollection<MenuP>();

            foreach (DataRow item in pData.Rows)
            {


                vMenu = pData.AsEnumerable().Select(x => new MenuP()
                {
                    menu = pHeader ? x.Field<string>("decripcionPadre") : x.Field<string>("Menu"),
                    descripcionPadre = x.Field<string>("decripcionPadre"),
                    menuHeader = x.Field<string>("Descripcion"),
                    subMenu = pHeader ? null : x.Field<string>("SubMenu"),
                    MenuItems = !string.IsNullOrEmpty(x.Field<string>("SubMenu")) ?
                               pData.ConvertToEnumarable().Where(y => y.subMenu == x.Field<string>("SubMenu")).ToObservableCollection() : null
                }).Where(x => x.menu == item.Field<string>("Menu") && x.menuHeader != item.Field<string>("Descripcion")).ToObservableCollection();
            }
            return vMenu;
        }

        public static ObservableCollection<MenuP> FillSubMenu<T>(this ObservableCollection<MenuP> pOrigen, List<MenuP> pData)
        {
            ObservableCollection<MenuP> vMenu = new ObservableCollection<MenuP>();
            foreach (MenuP item in pOrigen)
            {
                vMenu = pData.Where(x => x.menu == item.menu)
                             .Select(x => new MenuP()
                             {
                                 menu = x.menu,
                                 menuHeader = string.IsNullOrEmpty(x.subMenu) ? x.menuHeader : x.subMenu,
                                 subMenu = x.subMenu,
                                 NombreLogico = string.IsNullOrEmpty(x.subMenu) ? x.NombreLogico : null,
                                 MenuItems = !string.IsNullOrEmpty(x.subMenu) ?
                                            (new ObservableCollection<MenuP>()
                                            {
                                                new MenuP()
                                                {
                                                    menu = x.menu,
                                                    menuHeader = x.subMenu,
                                                    subMenu = null,
                                                    NombreLogico=null
                                                }
                                            }).FillSubMenu<MenuP>(pData.Where(z => z.subMenu == x.subMenu)
                                                                       .Select(xx => new MenuP()
                                                                       {
                                                                           menu = xx.menu,
                                                                           menuHeader = xx.menuHeader,
                                                                           descripcionPadre = xx.descripcionPadre,
                                                                           subMenu = (xx.subMenu == x.subMenu) ? null : x.subMenu,
                                                                           NombreLogico = xx.NombreLogico
                                                                       }).ToList()) :
                                                  null
                             }).DistinctBy(x => x.menuHeader)
                               .ToObservableCollection();
                pOrigen.Where(x => x.menu == item.menu).FirstOrDefault().MenuItems = vMenu;
            }



            return vMenu;
        }
    }

    public class MenuP
    {
      
        public string menu { get; set; }

        
        public string subMenu { get; set; } = string.Empty;

       
        public string menuHeader { get; set; }
        public string NombreLogico { get; set; }
        public string Orden { get; set; }
        public string Icono { get; set; }
        public Visibility iconMenuVisible { get; set; } = Visibility.Collapsed;


        public string descripcionPadre { get; set; }
        public ObservableCollection<MenuP> MenuItems { get; set; }
        public MenuP() { }

    }

    [ImplementPropertyChanged]
    public class MenuVM
    {
        public class MenuPTmp {
            public string menu { get; set; }
            public int Nivel { get; set; }
            public string subMenu { get; set; }
            public string menuHeader { get; set; }
            public string descripcionPadre { get; set; }

            public MenuPTmp() { }
        }
        #region Propiedades y Variables
        private int incremento = 0;
        private int hours = 0;
        private int minute = 0;
        const int GB = 1024;



        //public ObservableCollection<MenuPrincipal> MenuItems { get; set; }        
        public ObservableCollection<MenuP> MenuItems { get; set; }
        public string MemoriaEquipoDisponible { get; set; }
        public string MemoriaEquipoDisponiblePorc { get; set; }
        public string MemoriaEquipoOcupada { get; set; }
        public string MemoriaEquipoOcupadaPorc { get; set; }
        public string MemoriaEquipo { get; set; }


        private Orientation verticalMenuTradicional = Orientation.Horizontal;

        private bool ckverticalMenuTradicional { get; set; } = false;
        public bool ckVerticalMenuTradicional
        {
            get
            {
                return ckverticalMenuTradicional;
            }
            set
            {
                ckverticalMenuTradicional = value; VerticalMenuTradicional = value == false ? Orientation.Horizontal : Orientation.Vertical;
            }
        }
        public Orientation VerticalMenuTradicional
        {
            get
            {
                return this.verticalMenuTradicional;
            }
            private set
            {
                verticalMenuTradicional = value;
                TopMenuTradicional = verticalMenuTradicional == Orientation.Horizontal ? Dock.Top : Dock.Left;

            }
        }

        public Dock TopMenuTradicional { get; private set; } = Dock.Top;
        public ObservableCollection<Proceso> lsProcesos { get; set; }

        public MenuPrincipal SelectMenu { get; set; }
        public string startSesion { get { return DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"); } }
        public string duracionSesion { get; set; }
        public string menuHeader { get; set; }

        #region Datos del usuario
        public string User { get; set; }
        public string Nombre { get; set; }
        public string Ocupacion { get; set; }
        public string Departamento { get; set; }
        public string BaseDatos { get; set; }
        public string Email { get; set; }
        public string extencion { get; set; }
        public string Flota { get; set; }
        #endregion

        #region Estados Controles Usuario

        public bool ckTemaDark
        { get
            {
                // ModifyTheme(theme => theme.SetBaseTheme(AplicarTema ? MaterialDesignThemes.Wpf.Theme.Dark : MaterialDesignThemes.Wpf.Theme.Light));
                return AplicarTema;
            }
            set { AplicarTema = value; }
        }
        private bool AplicarTema { get; set; }

        public bool ckMenuConvencional {
            get
            {
                return visibleMenuConvencional == Visibility.Visible ? false : true;
            }
            set
            {
                visibleMenuArbol = value == false ? Visibility.Collapsed : Visibility.Visible;
                visibleMenuConvencional = value == false ? Visibility.Visible : Visibility.Collapsed;
                ckMenuConvencional2 = value == false ? true : false;
            }
        }
        public bool ckMenuConvencional2 { get; private set; } = true;
        public Visibility visibleMenuConvencional { get; set; }

        public Visibility visibleMenuArbol { get; set; } = Visibility.Collapsed;



        public bool ckUser { get { return visibleUser == Visibility.Visible ? true : false; } set { visibleUser = value == true ? Visibility.Visible : Visibility.Collapsed; } }

        public Visibility visibleUser { get; set; }


        public bool ckOcupacion { get { return visibleOcupacion == Visibility.Visible ? true : false; } set { visibleOcupacion = value == true ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility visibleOcupacion { get; set; }

        public bool ckDepartamento { get { return visibleDepartamento == Visibility.Visible ? true : false; } set { visibleDepartamento = value == true ? Visibility.Visible : Visibility.Collapsed; } }

        public Visibility visibleDepartamento { get; set; }

        public bool ckBaseDatos { get { return visibleBaseDatos == Visibility.Visible ? true : false; } set { visibleBaseDatos = value == true ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility visibleBaseDatos { get; set; }

        public bool ckEmail { get { return visibleEmail == Visibility.Visible ? true : false; } set { visibleEmail = value == true ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility visibleEmail { get; set; }

        public bool ckExtension { get { return visibleExtension == Visibility.Visible ? true : false; } set { visibleExtension = value == true ? Visibility.Visible : Visibility.Collapsed; } }
        public Visibility visibleExtension { get; set; }

        public bool ckModoNoturno { get { if (visibleModoNoturno == Visibility.Visible) ckTemaDark = false; return visibleModoNoturno == Visibility.Visible ? true : false; } set { visibleModoNoturno = value == true ? Visibility.Visible : Visibility.Collapsed; } }

        public Visibility visibleModoNoturno { get; set; }

        public bool ckMensajeria { get { return visibleMensajeria == Visibility.Visible ? true : false; } set { visibleMensajeria = value == true ? Visibility.Visible : Visibility.Collapsed; } }

        public Visibility visibleMensajeria { get; set; }

        public bool ckFoto { get { return visibleFoto == Visibility.Visible ? true : false; } set { visibleFoto = value == true ? Visibility.Visible : Visibility.Collapsed; } }

        public Visibility visibleFoto { get; set; }

        public Visibility LoadMenuTradicional { get; set; } = Visibility.Visible;



        public bool ckStartSesion { get { return visibleStartSesion == Visibility.Visible ? true : false; } set { visibleStartSesion = value == true ? Visibility.Visible : Visibility.Collapsed; } }

        public Visibility visibleStartSesion { get; set; }

        public bool ckDuracionSesion { get { return visibleDuracionSesion == Visibility.Visible ? true : false; } set { visibleDuracionSesion = value == true ? Visibility.Visible : Visibility.Collapsed; } }

        public Visibility visibleDuracionSesion { get; set; }
        #endregion



        public bool ckColorFavorito { get { return colorFavorito == Brushes.DarkBlue ? true : false; } set { colorFavorito = value == true ? Brushes.DarkBlue : Brushes.LightGray; } }

        public Brush colorFavorito { get; set; }

        #endregion


        List<MenuPTmp> menuItemsTmp { get; set; }
       public List<MenuP> lMenu { get; set; }


        #region Comandos
        public RelayCommand runAplicacion { get; private set; }

        private void RefrescarAplicaciones() {

            if (lsProcesos != null)
            {
                lsProcesos.Clear();

            }

            if (Procesos.lstProcesos != null) {
                ObservableCollection<Proceso> ltmp = new ObservableCollection<Proceso>();
                foreach (Process p in Procesos.lstProcesos)
                {
                    if (!p.HasExited)
                        ltmp.Add(new Proceso()
                        { Id = p.Id,
                            InicioProceso = p.StartTime,
                            NombreProceso = p.ProcessName,
                            NombreTituloProceso = p.MainWindowTitle,
                            CargaMemoria = (p.PagedMemorySize64 / GB).ToString(),
                            MaxMemoria = p.MaxWorkingSet.ToString(),
                            MinMemoria = p.MinWorkingSet.ToString()
                        });
                }
                lsProcesos = ltmp;
                Procesos.ActualizarLista(lsProcesos);
            }
        }


       
        #endregion
        public MenuVM()
        {
            
          


            #region agrupar
            RefrescarAplicaciones();

            DataTable dt = new DataTable();
            
            #region Definicion de la tabla
            dt.TableName = "dt_Menu_N";
            dt.Columns.Add(new DataColumn("Menu", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Descripcion", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("SubMenu", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("decripcionPadre", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Orden", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("comando", Type.GetType("System.String")));
            #endregion
           

            DataRow dr = dt.NewRow();

            #region Reportes

            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-1", dr["SubMenu"] = null,dr["decripcionPadre"]="Reporte",dr["Orden"]="4",dr["comando"]="notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-2", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-3", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-4", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-1", dr["SubMenu"] = "SubReporte", dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-5", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-6", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-7", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-8", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-9", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-10", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-11", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-13", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-1", dr["SubMenu"] = "SubReporte-1", dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-2", dr["SubMenu"] = "SubReporte-1", dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "REPO", dr["Descripcion"] = "Reporte-14", dr["SubMenu"] = null, dr["decripcionPadre"] = "Reporte", dr["Orden"] = "4", dr["comando"] = "notepad.exe");

            #endregion

            #region Mantenimiento

            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-1", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-2", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-3", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-4", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-1", dr["SubMenu"] = "SubMantenimiento", dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-1", dr["SubMenu"] = "SubMantenimiento2", dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-2", dr["SubMenu"] = "SubMantenimiento2", dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-3", dr["SubMenu"] = "SubMantenimiento2", dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-1", dr["SubMenu"] = "Yordani", dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-5", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-6", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-7", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-8", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-9", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-10", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-11", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-12", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-1", dr["SubMenu"] = "SubMantenimiento-1", dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "MANT", dr["Descripcion"] = "Mantenimiento-4", dr["SubMenu"] = null, dr["decripcionPadre"] = "Mantenimiento", dr["Orden"] = "1", dr["comando"] = "notepad.exe");

            #endregion

            #region Proceso

            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-1", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-2", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-3", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-4", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-1", dr["SubMenu"] = "SubProceso", dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-2", dr["SubMenu"] = "SubProceso", dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-3", dr["SubMenu"] = "SubProceso", dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-4", dr["SubMenu"] = "SubProceso", dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-5", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-6", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-7", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-8", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-9", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-10", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-11", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "PROC", dr["Descripcion"] = "Proceso-12", dr["SubMenu"] = null, dr["decripcionPadre"] = "Proceso", dr["Orden"] = "2", dr["comando"] = "notepad.exe");

            #endregion

            #region Consulta
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-1", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-2", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-3", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-4", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consult" +
                "a-1", dr["SubMenu"] = "SubConsulta", dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-2", dr["SubMenu"] = "SubConsulta", dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-3", dr["SubMenu"] = "SubConsulta", dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-4", dr["SubMenu"] = "SubConsulta", dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-5", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-6", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-7", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-8", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-1", dr["SubMenu"] = "SubConsulta2", dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-2", dr["SubMenu"] = "SubConsulta2", dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-3", dr["SubMenu"] = "SubConsulta2", dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-4", dr["SubMenu"] = "SubConsulta2", dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");

            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-9", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-10", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-11", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");
            dt.Rows.Add(dr["Menu"] = "CONS", dr["Descripcion"] = "Consulta-12", dr["SubMenu"] = null, dr["decripcionPadre"] = "Consulta", dr["Orden"] = "3", dr["comando"] = "notepad.exe");

            #endregion

            #endregion
            ObservableCollection<MenuP> SubMenu= new ObservableCollection<MenuP>();

            //Task.Run(() =>
            //{
                lMenu = dt.AsEnumerable().Select(x => new MenuP()
            {
                menu =   x.Field<string>("Menu"),
                descripcionPadre = x.Field<string>("decripcionPadre"),
                menuHeader = x.Field<string>("Descripcion"),
                subMenu =  x.Field<string>("SubMenu"),
                NombreLogico=x.Field<string>("comando"),
                Orden = x.Field<string>("Orden")
            }).ToList();

            dt.Clear();
            dt.Dispose();

                List<string> lIcon = new List<string>();
                lIcon.Add("Cogs");
                lIcon.Add("RegisteredTrademark");
                lIcon.Add("DatabaseSearch");
                lIcon.Add("ChartFinance");
                lIcon.Add("Windows");
                lIcon.Add("StarBorder");

                SubMenu = lMenu.OrderBy(x => x.Orden)
                               .Select(x=>new MenuP() { menu = x.menu, menuHeader=x.descripcionPadre ,NombreLogico=null})
                               .DistinctBy(x=>x.menu)
                               .ToObservableCollection();

                SubMenu.Add(new MenuP()
                {
                    menu = "WIND",
                    menuHeader="Ventanas",
                    Orden ="5"
                });

                SubMenu.Add(new MenuP()
                {
                    menu = "FAVO",
                    menuHeader = "Favoritos",
                    Orden = "6"
                });


                SubMenu = SubMenu.Select((x, key) => 
                { 
                    x.Icono = key+1<=lIcon.Count? lIcon[key]: "HelpNetwork";
                    x.iconMenuVisible = Visibility.Visible;
                    return x; 
                }).ToObservableCollection();

                //Añadiendo otros menues

                SubMenu.FillSubMenu<MenuP>(lMenu);
                    //Thread.Sleep(5000);
                MenuItems = SubMenu;
                LoadMenuTradicional = Visibility.Collapsed;
            //});



            #region Ejemplo Menu
            //MenuItems = new ObservableCollection<MenuPrincipal>
            //{
            //    new MenuPrincipal { menuHeader = "PROCESOS" ,iconMenu="Cogs",iconMenuVisible=Visibility.Visible},
            //    new MenuPrincipal { menuHeader = "REGISTROS",iconMenu="RegisteredTrademark", iconMenuVisible=Visibility.Visible},
            //    new MenuPrincipal { menuHeader = "CONSULTAS" ,iconMenu="DatabaseSearch", iconMenuVisible=Visibility.Visible,
            //        MenuItems = new ObservableCollection<MenuPrincipal>
            //            {
            //                new MenuPrincipal { menuHeader = "Consulta 1" ,visibleFavorito=Visibility.Visible},
            //                new MenuPrincipal { menuHeader = "Consulta 2",visibleFavorito=Visibility.Visible,
            //                    MenuItems = new ObservableCollection<MenuPrincipal>
            //                    {
            //                        new MenuPrincipal { menuHeader = "Consulta 2.1" },
            //                        new MenuPrincipal { menuHeader = "Consulta 2.2" },
            //                        new MenuPrincipal { menuHeader = "Consulta 2.3" }
            //                    }
            //                },
            //                new MenuPrincipal { menuHeader = "Consulta 3" },
            //                new MenuPrincipal { menuHeader = "Consulta 4" }

            //            }
            //    },
            //    new MenuPrincipal { menuHeader = "REPORTE",iconMenu="ChartFinance", iconMenuVisible=Visibility.Visible },
            //    new MenuPrincipal { menuHeader = "VENTANAS",iconMenu="Windows", iconMenuVisible=Visibility.Visible },
            //    new MenuPrincipal { menuHeader = "FAVORITOS",iconMenu="StarBorder", iconMenuVisible=Visibility.Visible } ,
            //};
            #endregion

            DispatcherTimer tm = new DispatcherTimer();
            tm.Interval = TimeSpan.FromSeconds(1);
            tm.Tick += Dt_Tick;
            tm.Start();

            

            runAplicacion = new RelayCommand(Execute);
        }

        #region Metodos
        PerformanceCounter memo = new PerformanceCounter("Memory", "Available MBytes");
        private void Dt_Tick(object sender, EventArgs e)
        {
            incremento++;
            if (incremento > 59)
            {
                incremento = 0;
                minute++;
                if (minute > 59)
                {
                    minute = 0;
                    hours++;
                }
            }
            
          
              MemoriaEquipoDisponible=(memo.NextValue() / GB).ToString("F");
              MemoriaEquipo = "16";
              MemoriaEquipoDisponiblePorc = (Convert.ToDecimal(MemoriaEquipoDisponible) / Convert.ToDecimal(MemoriaEquipo)*100).ToString("F");


            RefrescarAplicaciones();

            duracionSesion = $"{hours.ToString("00")}:{minute.ToString("00")}:{incremento.ToString("00")}";
        }


        private void Execute(object v)
        {
            var NombreLogico = ((TextBlock)v).Tag;
           
            if (NombreLogico!=null)
            {
                try
                {
                    MenuP menuP = lMenu.Where(x => x.NombreLogico.ToUpper() == NombreLogico.ToString().ToUpper()).FirstOrDefault();
                    Process p = Process.Start(menuP.NombreLogico);
                    Procesos.Agregar(p);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void OnExecuteChanged()
        {

            MessageBox.Show("Clicked at " + SelectMenu.menuHeader);
            //Process p = Process.Start("notepad.exe");
            //Procesos.Agregar(p);
        }

        private static void ModifyTheme(Action<ITheme> modificationAction)
        {
            PaletteHelper paletteHelper = new PaletteHelper();
            ITheme theme = paletteHelper.GetTheme();

            modificationAction?.Invoke(theme);

            paletteHelper.SetTheme(theme);
        }


        #endregion
    }
}
