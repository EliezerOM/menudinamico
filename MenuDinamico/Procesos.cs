﻿using MenuDinamico.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MenuDinamico
{
    public static class Procesos
    {
        public static ObservableCollection<Process> lstProcesos;

        public static void Agregar(Process p)
        {
            if(lstProcesos == null)
            {
                lstProcesos = new ObservableCollection<Process>();
            }

            lstProcesos.Add(p);
        }

        public static void ActualizarLista(ObservableCollection<Proceso> pProceso) {
            if (lstProcesos != null)
            {
                foreach (Process item in lstProcesos)
                {
                    if (!item.HasExited)
                    {
                        var proceso = Process.GetProcessById(item.Id);
                        if (proceso != null)
                        {
                            pProceso.Where(x => x.Id == proceso.Id)
                                .Select(upd =>
                            {
                                upd.NombreTituloProceso = proceso.MainWindowTitle;
                                upd.CargaMemoria = proceso.PagedSystemMemorySize64.ToString();
                                return upd;
                            }).ToList();

                        }
                    }
                      
                   
               
                }

            }
        }

        /// <summary>
        /// Cierra todas las aplicaciones
        /// </summary>
        /// <returns></returns>
        public static bool CerrarApp()
        {
            if (lstProcesos != null)
            {

                int id = 0;
                foreach (Process p in lstProcesos)
                {
                    if (!p.HasExited)
                    {
                        id = p.Id;
                        p.CloseMainWindow();

                    }
                }
            }
            return VerificaAplicacion();

        }

         /// <summary>
        /// Cierra una aplicaciones en especifico
        /// </summary>
        /// <returns></returns>
        public static void CerrarApp(int pId)
        {
            if (lstProcesos != null)
            {
                var process = lstProcesos.Where(x => x.Id == pId).FirstOrDefault();
                if (process != null)
                {
                 
                        if (!process.HasExited)
                        {
                        process.CloseMainWindow();
                        }
                    
                }
              
            }

        }

        /// <summary>
        /// Cierra de manera forsada todas las aplicaciones
        /// </summary>
        public static void KillApp()
        {
            if (lstProcesos != null)
            {

                
                foreach (Process p in lstProcesos)
                {
                    if (!p.HasExited)
                    {
                       
                        p.Kill();

                    }
                }
            }
        

        }
        /// <summary>
        /// Cierra de manera forsada una aplicación en especifico
        /// </summary>
        /// <param name="id"></param>
        public static void KillApp(int pId=0)
        {
            if (lstProcesos != null)
            {

                var process = lstProcesos.Where(x => x.Id == pId).FirstOrDefault();
                if (process != null)
                {
                    if (!process.HasExited)
                    {
                        process.Kill();
                    }
                }
            }


        }


        /// <summary>
        /// Verifica si existe alguna aplicacion en ejecucion, partiendo desde los procesos listados.
        /// </summary>
        /// <returns> valor booleano</returns>
        public static bool VerificaAplicacion()
        {
           
                bool estado = false;
            if (lstProcesos != null)
            {
                estado=lstProcesos.Where(x => x.HasExited == false).Count() > 0 ? true : false;
            
            }
            return estado;


        }

        /// <summary>
        /// Verifica si existe la aplicacion especificada en ejecucion, partiendo desde los procesos listados.
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public static bool VerificaAplicacion(int pId=0)
        {
            bool estado = false;
            if (lstProcesos == null) return estado;
            var process = lstProcesos.Where(x => x.Id == pId).FirstOrDefault();
            if (process!=null)
            {

                if (process.HasExited == false) { estado = true; }


            }

            return estado;


        }

    }
}
