﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace MenuDinamico.Models
{
   
    public class Proceso
    {
        public int Id { get; set; }
        public string NombreProceso { get; set; }
        public string NombreTituloProceso { get; set; }
        public string NombreProcesoId { get { return $"{Id}-{NombreProceso}/{NombreTituloProceso}"; } }
        public DateTime InicioProceso{ get; set; }
        public DateTime FinProceso{ get; set; }
        public string DuracionProceso{ get; set; }
        public string CargaMemoria { get; set; }
        public string MaxMemoria { get; set; }
        public string MinMemoria { get; set; }

        private int incremento;
        private int minute;
        private int hours;
        public Proceso() {

            DispatcherTimer tm = new DispatcherTimer();
            tm.Interval = TimeSpan.FromSeconds(1);
            tm.Tick += Dt_Tick;
            tm.Start();
        }
        private void Dt_Tick(object sender, EventArgs e)
        {
            incremento++;
            if (incremento > 59)
            {
                incremento = 0;
                minute++;
                if (minute > 59)
                {
                    minute = 0;
                    hours++;
                }
            }
            DuracionProceso = $"{hours.ToString("00")}:{minute.ToString("00")}:{incremento.ToString("00")}";
        }
    }
}
