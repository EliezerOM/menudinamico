﻿using MenuDinamico.comandos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MenuDinamico.Models
{

    
    public class MenuPrincipal
    {
        public ObservableCollection<MenuPrincipal> MenuItems { get; set; }
        
        public Comando runAplicacion { get; private set; }
        public string menuHeader { get; set; }
        public string iconMenu { get; set; }
        public string NombreLogico { get; set; }

        private Visibility visibilityFavorito = Visibility.Collapsed;
        public Visibility visibleFavorito { get { return visibilityFavorito; } set { visibilityFavorito = value; } }

        private Visibility visibility = Visibility.Collapsed;
        public Visibility iconMenuVisible { get { return visibility; } set { visibility = value; } }
        public MenuPrincipal() {

        }
       
    }
}
