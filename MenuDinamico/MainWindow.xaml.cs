﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using MaterialDesignThemes.Wpf;
using MaterialDesignColors;
using System.Text.RegularExpressions;
using LibraryEOM.Models;

namespace MenuDinamico
{
   
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new ViewModels.MenuVM();
        
            //LibraryEOM.Metodos.ThemeMethod.SetLightOrDark(Application.Current);
            

          
        }

        private void SetLightOrDark()
        {
            throw new NotImplementedException();
        }

        public void SetLightDark(ThemeRegister Theme)
        {
            if (Theme == null) return;
                              

                var resources = Application.Current.Resources.MergedDictionaries;

                if (resources == null) return;
                var existingResourceDictionary = Application.Current.Resources.MergedDictionaries
                                                .Where(rd => rd.Source != null)
                                                .SingleOrDefault(rd => Regex.Match(rd.Source.OriginalString, $@"({Theme.ThemeOriginal})").Success);



                if (existingResourceDictionary == null)
                    throw new ApplicationException("Unable to find Light/Dark base theme in Application resources.");

                var source = $"{Theme.ThemeReemplazar}";


                var newResourceDictionary = new ResourceDictionary() { Source = new Uri(source, Theme.TypeUri) };

                Application.Current.Resources.MergedDictionaries.Remove(existingResourceDictionary);
                Application.Current.Resources.MergedDictionaries.Add(newResourceDictionary);
            
        }

        public IEnumerable<Swatch> Swatches { get; }
        private static void ModifyTheme(Action<ITheme> modificationAction)
        {
            PaletteHelper paletteHelper = new PaletteHelper();
            ITheme theme = paletteHelper.GetTheme();

            modificationAction?.Invoke(theme);

            paletteHelper.SetTheme(theme);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
          
            LUser.IsRightDrawerOpen = true;
        }

        private void Button_Click2(object sender, RoutedEventArgs e)
        {

            LUser.IsTopDrawerOpen = true;
        }

        private void Chip_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
             e.Cancel = Procesos.CerrarApp();


        }

        private void checkn_Click(object sender, RoutedEventArgs e)
        {
            
            LibraryEOM.Metodos.ThemeMethod.SetLightOrDark(Application.Current, checkn.IsChecked.Value);

        }

        private void tema(bool state) {
            if (state)
            {
                ModifyTheme(theme => theme.SetBaseTheme(MaterialDesignThemes.Wpf.Theme.Dark));
                ModifyTheme(theme => theme.SetPrimaryColor(Colors.DarkBlue));
                ModifyTheme(theme => theme.SetSecondaryColor(Colors.Red));

            }
            else
            {
                ModifyTheme(theme => theme.SetBaseTheme(MaterialDesignThemes.Wpf.Theme.Light));
                ModifyTheme(theme => theme.SetPrimaryColor(Colors.DarkBlue));
                ModifyTheme(theme => theme.SetSecondaryColor(Colors.Red));

            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void BtnClickCambiarContrasena(object sender, RoutedEventArgs e)
        {
            //DialogCambiarContrasena.IsOpen = true;
        }

        private void CancelarCambiarContrasena(object sender, RoutedEventArgs e)
        {
            //DialogCambiarContrasena.IsOpen = false;
            //txtConfirmarContrasena.Password = string.Empty;
            //txtContrasenaActual.Password = string.Empty;
            //txtNuevaContrasena.Password = string.Empty;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            NewPass.IsOpen = true;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }
    }

    

  

}
